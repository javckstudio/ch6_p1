-----------------------------------------------------------------------------------------
--
-- reader.lua
--
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene( )
--=======================================================================================
--宣告各種變數
--=======================================================================================
local books = display.newGroup( )
local pageFlipSound = audio.loadSound( "PageFlip.mp3" )

local initial
local goBack
local goForward
--=======================================================================================
--定義各種函式
--=======================================================================================
initial = function ( parent )
	local img_page1 = display.newImageRect( parent, "Page1.png", _SCREEN.WIDTH, _SCREEN.HEIGHT )
	img_page1.x = _SCREEN.CENTER.X
	img_page1.y = _SCREEN.CENTER.Y
	books:insert( img_page1 )

	local img_page2 = display.newImageRect( parent, "Page2.png", _SCREEN.WIDTH, _SCREEN.HEIGHT )
	img_page2.x = _SCREEN.CENTER.X
	img_page2.y = _SCREEN.CENTER.Y
	img_page2.isVisible = false
	books:insert( img_page2 )

	btn_back = widget.newButton{
		width = 81,
		height = 81,
		defaultFile = "BackButton.png",
		overFile = "BackButtonPressed.png",
		onEvent = goBack
	}
	btn_back.x = 235
	btn_back.y = 895
	btn_back.isVisible = false

	btn_forward = widget.newButton{
		width = 81,
		height = 81,
		defaultFile = "ForwardButton.png",
		overFile = "ForwardButtonPressed.png",
		onEvent = goForward
	}
	btn_forward.x = 423
	btn_forward.y = 895
	btn_forward.isVisible = true

end

goBack = function ( event )
	audio.play( pageFlipSound )
	books[2].isVisible = false
	btn_forward.isVisible = true
	btn_back.isVisible = false
	
end

goForward = function ( event )
	audio.play( pageFlipSound )
	books[2].isVisible = true
	btn_back.isVisible = true 
	btn_forward.isVisible = false
end
--=======================================================================================
--Composer
--=======================================================================================

--畫面沒到螢幕上時，先呼叫scene:create
--任務:負責UI畫面繪製
function scene:create(event)
	--把場景的view存在sceneGroup這個變數裡
	local sceneGroup = self.view

	--接下來把會出現在畫面的東西，加進sceneGroup裡面
	initial(sceneGroup)
end


--畫面到螢幕上時，呼叫scene:show
--任務:移除前一個場景，播放音效，開始計時，播放各種動畫
function  scene:show( event)
	local sceneGroup = self.view
	local phase = event.phase

	if( "will" == phase ) then
		--畫面即將要推上螢幕時要執行的程式碼寫在這邊
	elseif ( "did" == phase ) then
		--把畫面已經被推上螢幕後要執行的程式碼寫在這邊
		--可能是移除之前的場景，播放音效，開始計時，播放各種動畫

	end
end


--即將被移除，呼叫scene:hide
--任務:停止音樂，釋放音樂記憶體，停止移動的物體等
function scene:hide( event )
	
	local sceneGroup = self.view
	local phase = event.phase

	if ( "will" == phase ) then
		--畫面即將移開螢幕時，要執行的程式碼
		--這邊需要停止音樂，釋放音樂記憶體，有timer的計時器也可以在此停止

	elseif ( "did" == phase ) then
		--畫面已經移開螢幕時，要執行的程式碼
	end
end

--下一個場景畫面推上螢幕後
--任務:摧毀場景
function scene:destory( event )
	if ("will" == event.phase) then
		--這邊寫下畫面要被消滅前要執行的程式碼

	end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destory", scene )

return scene