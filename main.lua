-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------


--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )
local composer = require( "composer")
--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}

--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	composer.gotoScene( "mainMenu" )
end

--=======================================================================================
--定義其他函式
--=======================================================================================

--=======================================================================================
--呼叫主函式
--=======================================================================================
main()